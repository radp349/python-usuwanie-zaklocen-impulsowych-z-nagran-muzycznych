[Signal, Fs] = audioread('02_nasz.wav');
y = Signal';
yfilt = y;

N = 30000;
lambda = 0.99;%--------!

nA=4;%--------!
nSD = 50;%--------!
teta=zeros(nA,1);
P=1000*eye(nA);
teta_s=zeros(nA,N);
e = zeros(1,N);
eSDev = 4; %--------!
eProp = 1;
counter = 0;


for t=nA+1:N
    
    if t == nA+1
        fi=[y(t-1:-1:t-nA)]';
    end


    e(t)=y(t)-fi'*teta;
    if t == nSD+1
        eProp = e(t-nSD:t-1);
    elseif t >= nSD+1
        eSDev = std(eProp);
    end

    if abs(e(t)) < 5*eSDev || counter > 4 || counter < 0
        k=P*fi/(lambda+fi'*P*fi);
        eProp = [eProp(2:length(eProp)), e(t)];
        P=(P-k*fi'*P)/lambda; 
        fi = [y(t); fi(1:length(fi)-1)];
        if counter < 0 && counter > -5
            counter = counter-1;
        elseif counter > 4
            disp(t)
            P = 1000*eye(nA);
            counter = -1;
        else
            counter = 0;
        end
    else
        k = 0;
        yfilt(t) = fi'*teta;
        P=(P-k*fi'*P)/lambda; 
        fi = [yfilt(t); fi(1:length(fi)-1)]; %???
        counter = counter + 1;
    end
    teta=teta+k*e(t);
    teta_s(:,t)=teta;
    

%     if t>= 7400
%         disp('xD')
%     
%                 h1 = subplot(2,2,1);
%         cla(h1)
%         plot(teta_s(1,1:t));
%         hold on;
%         plot(e(1:t));
%         hold off
%         % axis([1,N,0,1]);
%         
% 
%         h2 = subplot(2,2,2);
%         cla(h2)
%         plot(teta_s(2,1:t));
%         hold on;
%         plot(e(1:t));
%         hold off
%         % axis([1,N,-1,0]);
% 
%         h3 = subplot(2,1,2);
%         cla(h3)
%         plot(y(1:t))
%         hold on
%         plot(yfilt(1:t))
%         hold off
%         % axis([1,N,-1,0]);
% 
%     end

end

plot(y(1:N))
hold on
plot(yfilt(1:N))

%sound(yfilt,Fs)