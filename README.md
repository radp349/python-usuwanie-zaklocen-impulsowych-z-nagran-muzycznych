# Python - Usuwanie zakłóceń impulsowych z nagrań muzycznych (Removing impulse noise from music recordings)

W plikach typu ‘wav’ zapisane są ok. 20 sekundowe fragmenty nagrań muzycznych z dodanymi (sztucznie) zakłóceniami impulsowymi (próbki reprezentowana przez 16-bitowe słowa, częstotliwość próbkowania 22050 Hz). 

The 'wav' files store about 20-second fragments of music recordings with added (artificially) impulse interference (samples represented by 16-bit words, sampling frequency 22050 Hz). 


## Procedura eliminacji zakłóceń (Interference elimination procedure):
* a) Przyjęty zostaje model autoregresyjny rzędu r (r = 4) do lokalnej reprezentacji dynamiki sygnału.

* a) An autoregressive model of order r (r = 4) is adopted for the local representation of signal dynamics.

* b) Poprzez zastosowanie algorytmu ważonych najmniejszych kwadratów (EW-LS) na bieżąco śledzone są zmiany parametrów ai (i = 1 , ... , r) modelu AR(r). Zastosowano detektor zakłóceń impulsowych kwestionujący w każdym kroku algorytmu EW-LS te próbki y(k) sygnału, dla których bezwzględna wartość błędu predykcji przekracza kilkukrotnie lokalną wartość średniego odchylenia standardowego błędu resztowego.

* b) By applying the weighted least squares (EW-LS) algorithm, changes in the parameters ai (i = 1 , ... , r) of the AR(r) model are tracked in real time. An impulse noise detector is used that challenges in each step of the EW-LS algorithm those samples y(k) of the signal for which the absolute value of the prediction error exceeds several times the local value of the average standard deviation of the residual error.

* c) Wykorzystywana jest metoda interpolacji liniowej do rekonstrukcji próbek zakwestionowanych przez detektor (zakwestionowaną próbkę y*(k) zastąpić średnią arytmetyczną sąsiednich, poprawnych próbek sygnału): y*(k) ¬ 0.5 [ y(k – 1) + y(k + 1)].

* (c) A linear interpolation method is used to reconstruct the samples challenged by the detector (replace the challenged sample y*(k) by the arithmetic mean of adjacent correct samples of the signal): y*(k) ¬ 0.5 [ y(k - 1) + y(k + 1)].

### Implementując procedurę usuwania trzasków przyjęto założenie, że w przetwarzanych danych mogą wystąpić serie zakłóceń obejmujące maksymalnie 4 próbki.

### Implementing the crackle removal procedure, the assumption was made that a series of disturbances involving a maximum of 4 samples could occur in the processed data.

![](Figure_1.png)

