import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt
import simpleaudio as sa

# Wczytanie pliku audio
Fs, Signal = wav.read('02_nasz.wav')
y = Signal.astype(float).T  # Konwersja na float
yfilt = y.copy()

# Parametry
N = len(Signal)
lambda_ = 0.99
nA = 4
nSD = 50
teta = np.zeros(nA).T
P = 1000*np.eye(nA)
teta_s = np.zeros((nA,N))
e = np.zeros(N)
eSDev = 5
eProp = np.array([1])
counter = 0

for t in range (nA,N):
    if t % int(N/10) == 0:
        print(round(100*t/N),'%')
    if t == nA:
        fi = np.array(y.tolist()[nA-1:0:-1] + [y[0]]).T
        print(fi)
    e[t] = y[t] - np.dot(fi.T,teta)
    if t == nSD: 
        eProp = np.array(e[t-nSD:t-1])
    elif t >= nSD:
        eSDev = np.std(eProp)

    if ((abs(e[t]) < 5*eSDev) or (counter > 4) or (counter < 0)):
        k = np.dot(P,fi) / (lambda_ + np.dot(np.dot(P,fi.T),fi))
        eProp = np.array(eProp.tolist()[1:] + [e[t]])
        P = (P-np.dot(np.dot(k,fi.T),P))/lambda_
        fi = np.array([y[t]] + fi.tolist()[:-1])
        
        if ((counter < 0) and (counter > -5)):
            counter -= 1
        elif counter > 4:
            counter = -1
        else:
            counter = 0
    else:
        k = 0
        # yfilt[t] = np.dot(fi.T,teta)
        yfilt[t] = (yfilt[t-1]+y[t+1])/2
        P = (P - np.dot(np.dot(k,fi.T),P))/lambda_
        fi = np.array([yfilt[t]] + fi.tolist()[:-1])
        counter += 1
    teta += k*e[t]
    teta_s[:, t] = teta

fig, axs = plt.subplots(2, 1, figsize=(8, 6))
axs[0].plot(y, linewidth=1.0)
axs[0].plot(yfilt, linewidth=0.5)
axs[0].set_title("Timelines of the recordings")
axs[0].legend(["Noised recording","Filtered recording"])
axs[0].set_xlabel('Time')
axs[0].set_ylabel('Signal value')
axs[1].set_title("Timelines of the regression vector parameters")
axs[1].plot(teta_s[0, :], linewidth=1.0)
axs[1].plot(teta_s[1, :], linewidth=1.0)
axs[1].plot(teta_s[2, :], linewidth=1.0)
axs[1].plot(teta_s[3, :], linewidth=1.0)
axs[1].set_xlabel('Time')
axs[1].set_ylabel('Parameter value')
axs[1].legend(["y[t-1]","y[t-2]","y[t-3]","y[t-4]"])
plt.tight_layout()
plt.show()

# Zapisanie przefiltrowanego dźwięku do pliku WAV
wav.write("przefiltrowany_dzwiek.wav", Fs, yfilt.astype(np.int16))

# Odtworzenie przefiltrowanego dźwięku
wave_obj = sa.WaveObject.from_wave_file("przefiltrowany_dzwiek.wav")
play_obj = wave_obj.play()
play_obj.wait_done()
print("już")